# Prerequisites
- [node.js]

# To get started

If you haven't setup your environment do `cd backend && npm install` to install all dependencies in the root directory.

## Run Project

Run node server
```
cd backend && nodemon server
```
Open with a browser the file : 
```
site/index.html
```

